
* [Application Specs](#application-specs)
* [Answer Considerations](#answer-considerations)
* [Setup](#setup)
  * [Requirements](#requirements)
  * [Dependencies](#dependencies)
* [Usage](#usage)
* [License](#license)



### Application Specs

1. retrieves customer & order data from the Shopify API
1. calculates a return rate for a customer
    * Calculation Logic: total amount refunded / total amount ordered
1. if return rate
  * '< 50% then classification A'
  * '> 50% then classification B'
1. write back the classification as a tag to the customer in Shopify
1. write the customer-id, the classification with timestamp to a database to enable historical tracking of the customer classification


### Answer Considerations

Shortly draw the architecture you build the system on
decide on a tech stack and mention why you choose for specific language / service / infrastructure
to reduce complexity you can deploy the application locally
share your code with me, when done

Node.js with an Express server. MongoDB with Mongoose as an object modeler.

#### Why Node.js?
Elected to use Node.js with an Express server and a MongoDB back-end on this project for several reasons:
First and foremost, it allows for the use of only one language with the entire stack. and any developers
who need to work with the API
The API will likely need to interact with several external APIs in the near future, making an asynchronous implementation particularly valuable.
JavaScript's async-await syntax is simple and involves very little configuration compared to asyncio/uvloop/Tornado in Python.

#### Why Express?
Koa has a slight speed advantage over Express and embraces newer JavaScript features such as promises and generators.
While Express' emphasis on callbacks can lead to messier code:
* it is familiar to a greater number of developers
* has a more fully-developed ecosystem of middleware
* can be placed behind an NGINX reverse proxy, making the speed gap with Koa largely irrelevant

#### Why MongoDB?
MongoDB is versatile and allows for rapid development and iteration; its use of JSON allows for ease-of-use with Node.js.
However, it might not be the best choice in a production environment:
* Uses more storage than many other DBs in many cases and does not automatically perform disk cleanup
* Tendency to hold large amounts of duplicate data
* Join syntax can be tedious
* Fast, but only if indexing is done properly

Example URL:
https://{key}:{secret}@{org}.myshopify.com/admin/{target}.json

Endpoints of Interest:
[/admin/orders.json](https://help.shopify.com/en/api/reference/orders/order#index)
[/admin/customers.json](https://help.shopify.com/en/api/reference/customers/customer)
[/admin/customers/#{customer_id}.json](https://help.shopify.com/en/api/reference/customers/customer#show)
[/admin/customers/#{customer_id}/orders.json](https://help.shopify.com/en/api/reference/customers/customer#orders)

Used to call GET /customers/
`updated_at_min` - shows customers updated after specified date
`limit` - defaults to 50, max 250
`page` - defaults to 1
`fields` - restrict returned fields

Returned by customers:
`total_spent`

## Setup

### Requirements

1. Node.js @> 7.6, [npm](https://www.npmjs.com/get-npm) || [yarn](https://www.yarnpkg.com)
1. [MongoDB](https://www.mongodb.com/download-center/community) @ 4.0


### Dependencies

* [axios](https://github.com/axios/axios) - promise-based HTTP client
* [babel](https://www.npmjs.com/package/@babel/cli) - transpiles code for compatibility with older node clients
* [body-parser](https://www.npmjs.com/package/body-parser) - parses request bodies before sending them to handlers
* [chalk](https://www.npmjs.com/package/chalk) - colorizes terminal output
* [compression](https://www.npmjs.com/package/compression) - adds zlib compression
* [concurrently](https://www.npmjs.com/package/concurrently) - gracefully runs multiple npm scripts simultaneously
* [cross-env](https://www.npmjs.com/package/cross-env) - adds cross-platform support
* [dotenv](https://www.npmjs.com/package/dotenv) - read environmental variables from a .env file
* [errorhandler](https://www.npmjs.com/package/errorhandler) - exposes stack traces and internal details in **dev**
* [eslint](https://www.npmjs.com/package/eslint), [prettier](https://www.npmjs.com/package/eslint-plugin-prettier) - for enforcing style
* [express](https://www.npmjs.com/package/express) - minimalist server
* [helmet](https://www.npmjs.com/package/helmet) - sets HTTP headers for Express apps
* [mongoose](https://www.npmjs.com/package/mongoose) - MongoDB object modeling tool
* [morgan](https://www.npmjs.com/package/morgan) - for HTTP logging in development and production
* [nodemon](https://www.npmjs.com/package/nodemon) - node.js monitoring tool
* [opn](https://www.npmjs.com/package/opn) - cross-platform opener
* [winston](https://www.npmjs.com/package/winston) - for logging in production environments; links with morgan

See [package.json](package.json) for further reference.

### Usage

#### Development

1. Run `npm run dev` || `yarn dev`

#### Production

1. Run `npm start` || `yarn start`

### License

This project is licensed under the GPLv3 - see the [LICENSE.md](LICENSE.md) file for more details.
