### Authentication

Assume Private App rather than Public - no access to the JS SDK.

Authentication approach dependent on HTTP client choice - might be able to prepend `username:password@`

If not, can provide credentials in the Authorization header:
1. Join key and secret with a colon
1. Encode result in base64
1. Prepend the result with Basic and a space.

Example:
```
Authorization: Basic NDQ3OGViN2FjMTM4YTEzNjg1MmJhYmQ4NjE5NTZjMTk6M2U1YTZlZGVjNzFlYWIwMzk0MjJjNjQ0NGQwMjY1OWQ=
```

Neither of these options seem particularly secure.

### Shopify Resources
Example URL: https://fbc5a40a7274c526bdd7a2fbb854d136:1d5b4501917f925fd0500f0d82e45bda@ivy-oak-de.myshopify.com/admin/orders.json

https://fbc5a40a7274c526bdd7a2fbb854d136:1d5b4501917f925fd0500f0d82e45bda@ivy-oak-de.myshopify.com/admin/orders.json?financial_status=refunded

https://help.shopify.com/en/api/reference/orders/order
<!-- only the last 60 days of orders available -->
all orders available for private app
read access only

All resources suffixed with .json unless otherwise indicated

Customers resource accepts `fields` in GET requests to limit returned values. List must be comma-separated

| Resource  | Get  | Notes  |
|---|---|---|
| /orders  | Retrieves a list of orders  |   |
|  |   |   |
| /customers  |   |   |
| /admin/customers/#{customer_id}/orders  |  Retrieves all orders belonging to a customer |   |
|   |   |   |
|   |   |   |

"financial_status" - possible values:
* paid
* refunded
* partially_refunded

orders.json?updated_at_min=2005-07-31T15:57:11-04:00

/admin/orders.json?fields=created_at,id,name,total-price

https://help.shopify.com/en/api/reference/customers/customer

Customer Resource
"id"

