import bodyParser from "body-parser";
import compression from "compression";
import errorhandler from "errorhandler";
import express from "express";
import helmet from "helmet";
import morgan from "morgan";

// import environmental variables
import config from "./lib/config";

// colorization for console statements in terminal
import { failure, info, success } from "./lib/chalkConfig";
import logger from "./lib/logger";

import run from "./models";

const app = express();

// add middleware
app.use(bodyParser.json());
app.use(compression());
app.use(morgan("combined", { stream: logger.stream }));
app.use(helmet());

if (process.env.NODE_ENV === "development") {
  app.use(errorhandler());
}

app.get("/", (req, res) => res.status(200).send("Connected to server successfully!"));

// development error handler
// will print stack trace
if (app.get('env') === 'development') {
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err,
    });
  });
}

// production error handler
// no stack traces leaked to user
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: {},
  });
});

export default app;
