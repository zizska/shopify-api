import dotenv from "dotenv";
import { success, failure, info } from "./chalkConfig";

// import credentials from .env file
// const config = dotenv.config({ debug: process.env.DEBUG });
const config = dotenv.config({ debug: process.env.DEBUG });
if (config.error) {
  failure("Invalid Shopify credentials supplied! Check your .env file");
  throw config.error;
} else {
  info(
    `Shopify credentials found. Using url ${process.env.URL} and database ${process.env.mongodb}`,
  );
}

export default config;
