import chalk from "chalk";

export const success = msg => console.log(chalk.green(msg));

export const failure = msg => console.error(chalk.red(msg));

export const info = msg => console.info(chalk.blue(msg));
