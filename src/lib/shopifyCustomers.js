import axios from "axios";
import { failure, info, success } from "./chalkConfig";
// /admin/customers.json
// /admin/customers/#{customer_id}.json
// /admin/customers/#{customer_id}/orders.json

const shopifyCustomerUri = `https://${process.env.key}:${process.env.secret}@${
  process.env.URL
}/admin/customers`;

// GET /admin/customers.json?updated_at_min=2018-10-22 19:42:02
// (format: 2014-04-25T16:15:47-04:00)
// don't limit date range for initial population
const customersFieldLimiter = ["limit=250", "updated_at_min"].join("&");

/*
API helper function
*/
export async function retrieveCustomerData(endpoint) {
  try {
    // add logic to request multiple pages
    const response = await axios.get(`${endpoint}.json`);
    success(response);
  } catch (error) {
    failure(error);
  }
}

/* GET /admin/customers.json - retrieves all customers
fields of interest: id
*/

export async function getCustomers() {
  const response = await retrieveCustomerData(`${shopifyCustomerUri}`);
  return response;
}

/* GET /admin/customers/#{customer_id}/orders.json - retrieves all orders belonging to a customer

iterate through transaction array
store value of amount key
currency field in beta; disregard for now
look for key-value pair "kind": "refunds"

*/

export async function getCustomerOrders(customerId) {
  const response = await retrieveCustomerData(`${shopifyCustomerUri}/${customerId}/orders`);
  return response;
}
