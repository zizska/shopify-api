import mongoose from "mongoose";

const Customer = mongoose.model(
  "Customer",
  new mongoose.Schema({
    customer_id: { type: Number, required: true },
    email: { type: String, required: true, unique: true },
    created_at: Date,
    orders: [Number],
    total_purchases: Number,
    total_refunds: Number,
    tags: [String],
  }),
);

export default Customer;
