import mongoose from "mongoose";

import { failure, info, success } from "../lib/chalkConfig";
import Customers from "./customers";

const mongodb = process.env.mongodb || "mongodb://localhost:27017";
const options = {
  useNewUrlParser: true,
};

async function run() {
  // mongoose handles connection await internally
  await mongoose.connect(
    mongodb,
    options,
  );
  const db = mongoose.connection;
  db.on("error", () => failure("Database connection error"));
  success(`Database connection successful`);
  // empty database
  await db.dropDatabase();
  await Customers.create({
    id: 12345,
    email: "test@test.com",
  });
  return db;
}

run().catch(error => failure(error.stack));

export default run;
