import run from "../models";
import { failure, info, success } from "../lib/chalkConfig";
import Customer from "../models/customers";
import { getCustomers, getCustomerOrders } from "../lib/shopifyCustomers";

async function populateCustomers() {
  try {
    const shopifyResponse = await getCustomers();
  } catch (error) {
    failure(error);
  }
}

// async function saveCustomers(customers) {
//   try {
//     customers.forEach(async customer => {
//       const { customer_id, email, ...args } = customer;
//       const alreadyExists = await run(Customers.findOne({ customer_id }));
//       if (!alreadyExists) {
//         const newCustomer = new Customer({
//           customer_id,
//           email,
//           args,
//         });
//         const saveCustomer = await newCustomer.save();
//       }
//     });
//   } catch (error) {
//     failure(error);
//   }
// }

async function saveCustomer(customer) {
  const { ...args } = customer;
  try {
    const newCustomer = new Customer({
      ...args,
    });
    info(`Saving new customer ${json.stringify(newCustomer)}`);
    const dbResponse = await newCustomer.save();
    return dbResponse;
  } catch (error) {
    failure(error);
  }
}

async function updateCustomer(customer) {
  try {
    findOneAndUpdate(
      { customer_id: customer.customer_id },
      { updated_at: customer.updated_at, orders_count: customer.orders_count },
      true,
    );
  } catch (err) {
    failure(err);
    next(err);
  }
}

const checkIfUpdated = (dbCustomer, shopifyCustomer) => {
  if (
    dbCustomer.updated_at === shopifyCustomer.updated_at &&
    dbCustomer.orders_count === shopifyCustomer.orders_count
  ) {
    return false;
  }
  return true;
};

async function checkIfExists(customer) {
  try {
    const { customer_id, email, updated_at, ...args } = customer;
    const dbResponse = await run(Customers.findOne({ customer_id }));
    if (dbResponse) {
      const response = checkIfUpdated(dbResponse);
      if (response) {
        updateCustomer(dbResponse);
      }
    } else {
      saveCustomer(customer);
    }
    return dbResponse;
  } catch (error) {
    failure(error);
    return next(err);
  }
}

/**
 * @param {Object[]} customers - Customer data from Shopify API
 * @param {!number} customers[].customer_id
 * @param {string} customers[].email
 */
async function saveCustomers(customers) {
  try {
    customers.forEach(async customer => {
      const checkCustomer = await checkIfExists(customer)
      checkIfUpdated(checkCustomer);
    });
  } catch (err) {
    failure(err);
    return next(err);
  }
}
