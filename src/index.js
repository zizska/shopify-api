import app from "./app";
import { success, info } from "./lib/chalkConfig";

const port = process.env.PORT || 3000;

const server = app.listen(port, () => info(`App listening on port ${port}`));

export default server;
