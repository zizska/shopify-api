module.exports = {
  "extends": ["airbnb-base", "prettier"],
  "plugins": ["prettier"],
  "rules": {
    "prettier/prettier": ["error"],
    "no-unused-vars": "off",
    "no-undef":"warn",
    "camelcase":"off",
  }
}
